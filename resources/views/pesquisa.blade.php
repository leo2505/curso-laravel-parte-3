<html>
    <head>
        <title>Busca por Cliente</title>
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <style>
            html, body{
                margin:0; padding: 0; box-sizing: border-box;
            }
        </style>
    </head>
    <body>
        <div class="main">
            <div class="container">
                <div class="row">
                    <div class="container col-md-8 offset-md-2">
                        <div class="card border">
                            <div class="card-header">   
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm">
                                    <div class="card-title">Cadastro de Cliente</div>
                                    </div>
                                    <div class="col-sm">
                                        <form class="navbar-form navbar-left" role="search" action="/pesquisar" method="post">
                                            @csrf
                                            <div class="form-group">
                                              <input type="text" name="pesquisa" class="form-control" placeholder="Pesquisar">
                                            </div>
                                           
                                        </form>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered table-hover" id="tabelaprodutos">
                                    <thead>
                                        <tr>
                                            <th>Cód.</th>
                                            <th>Nome</th>
                                            <th>Endereço</th>
                                            <th>Idade</th>
                                            <th>Email</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            @foreach ($clientes as $item)
                                            <tr>
                                                <td>{{ $item->id }}</td>
                                                <td>{{ $item->nome }}</td>
                                                <td>{{ $item->endereco }}</td>
                                                <td>{{ $item->idade }}</td>
                                                <td>{{ $item->email }}</td>
                                            </tr>
                                            @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>