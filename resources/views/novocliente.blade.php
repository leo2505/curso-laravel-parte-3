<html>
    <head>
        <title>Cadastro Cliente</title>
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <style>
            html, body{
                margin:0; padding: 0; box-sizing: border-box;
            }
        </style>
    </head>
    <body>
        <div class="main">
            <div class="container">
                <div class="row">
                    <div class="container col-md-8 offset-md-2">
                        <div class="card border">
                            <div class="card-header">
                                <div class="card-title">Cadastro de Cliente</div>
                            </div>
                            <div class="card-body">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <form action="/" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <label for="nome">Nome</label>
                                        <input type="text" name="nome" id="nome" class="form-control @error('nome') is-invalid @enderror" placeholder="Nome do Cliente">
                                        @error('nome')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="idade">Idade</label>
                                        <input type="text" name="idade" id="idade" class="form-control" placeholder="Idade" value="{{ old('idade', '')}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="endereco">Endereço</label>
                                        <input type="text" name="endereco" id="endereco" class="form-control" placeholder="Endereço">  
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" name="email" id="email" class="form-control" placeholder="Email">
                                    </div>
                                    <button class="btn btn-primary btn-sm" type="submit">Enviar</button>
                                    <button class="btn btn-primary btn-sm" type="cancel">Cancelar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>