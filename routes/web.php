<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'ClienteControlador@index')->name('Index');
Route::get('/novo', 'ClienteControlador@create')->name('Create');
Route::post('/', 'ClienteControlador@store')->name('postclientes');
Route::any('/pesquisar', 'ClienteControlador@pesquisar')->name('anypesquisar');
    //Nao dar return das rotas aqui... colocar no controller o return das views na index, edit, update...